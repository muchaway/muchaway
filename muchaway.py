#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# MuchAway

# Copyright 2015 Simó Albert i Beltran

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from configparser import ConfigParser
from wsgiref.simple_server import make_server
from base64 import b64decode
from pymysql import connect
from subprocess import check_output

CONFIG = {}
CONFIG['MuchAway'] = {}
CONFIG['MuchAway']['ConfigDir'] = '/etc/muchaway'
CONFIG['MuchAway']['ConfigFile'] = '{}/muchaway.ini'.format(CONFIG['MuchAway']['ConfigDir'])
CONFIG['MuchAway']['Port'] = 443
CONFIG['MuchAway']['CertFile'] = '{}/server.pem'.format(CONFIG['MuchAway']['ConfigDir'])
CONFIG['MuchAway']['KeyFile'] = '{}/server.key'.format(CONFIG['MuchAway']['ConfigDir'])
CONFIG['MuchAway']['MailPath'] = '/srv/mail'
CONFIG['MuchAway']['NotmuchConfig'] = '.notmuch-config'
CONFIG['MuchAway']['NotmuchCmd'] = 'notmuch'
CONFIG['MuchAway']['AuthBackend'] = 'MySQL'
CONFIG['MySQL'] = {}
CONFIG['MySQL']['DataBase'] = 'accounts'
CONFIG['MySQL']['Table'] = 'accounts'
CONFIG['MySQL']['FieldUser'] = 'user'
CONFIG['MySQL']['FieldDomain'] = 'domain'
CONFIG['MySQL']['FieldPassword'] = 'password'

class MuchAwayAuthMySQL():

    def __init__(self, config):
        self.config = config

    def _get_config(self, attr):
        return self.config.get(attr, 'MySQL')

    def _authenticated(self, email, password):
        self.user, self.domain = email.split('@', 1)
        pre_sql = "select true from {0} where {1} = '{2}' and {3} = '{4}' and {5} = encrypt('{6}',{5});"
        sql = pre_sql.format(
            self._get_config('Table'),
            self._get_config('FieldUser'),
            self.user,
            self._get_config('FieldDomain'),
            self.domain,
            self._get_config('FieldPassword'),
            password
        )
        connection = connect(
            host=self._get_config('Host'),
            user=self._get_config('User'),
            passwd=self._get_config('Password'),
            db=self._get_config('DataBase')
        )
        cursor = connection.cursor()
        cursor.execute(sql)
        result = cursor.fetchone()
        connection.close()
        return result

class MuchAwayApp():

    def __init__(self, config):
        self.config = config
        pre_auth_backend_statement = "MuchAwayAuth{}(self.config)"
        auth_backend_statement = pre_auth_backend_statement.format(self.config.get('AuthBackend'))
        self.auth_backend = eval(auth_backend_statement)

    def __call__(self, environ, start_response):
        self.environ = environ
        if not self._authenticated(self.environ.get('HTTP_AUTHORIZATION')):
            status, headers, content = self._login()
        else:
            status, headers, content = self._run_notmuch()
        headers.append(('Content-Length', str(len(content))))
        start_response(status, headers)
        yield content

    def _authenticated(self, header):
        if not header:
            return False
        _, encoded = header.split(None, 1)
        decoded = b64decode(encoded).decode('UTF-8')
        try:
            username, password = decoded.split(':', 1)
            return self.auth_backend._authenticated(username, password)
        except Exception as e:
            print(e)
            return False

    def _login(self):
        status = '401 Authentication Required'
        headers = [('Content-Type', 'text/html'), ('WWW-Authenticate', 'Basic realm="Login"')]
        return status, headers, bytes(status, 'utf8')

    def _get_config_file(self):
        return '{0}/{1}/{2}/{3}'.format(
            self.config.get('MailPath'),
            self.auth_backend.domain,
            self.auth_backend.user,
            self.config.get('NotmuchConfig')
        )

    def _is_json_requested(self, cmd):
        for param in cmd[3:]:
            if param[:2] != '--':
                return False
            if param == '--format=json':
                return True

    def _run_notmuch(self):
        try:
            config_file = self._get_config_file()
            config = '--config={0}'.format(config_file)
            cmd = [ self.config.get('NotmuchCmd') ]
            cmd.append(config)
            cmd.extend(self.environ['PATH_INFO'][1:].split())
            output = check_output(cmd)
            status = '200 OK'
            if self._is_json_requested(cmd):
                headers = [('Content-type', 'application/json; charset=utf-8')]
            else:
                headers = [('Content-type', 'text/plain; charset=utf-8')]
            return status, headers, output
        except Exception as e:
            print(e)
            status = '500 Internal Error'
            return status, [], bytes(status, 'utf8')

class MuchAwayConfig:

    def __init__(self):
        self.config = ConfigParser()
        self.config.read(CONFIG['MuchAway']['ConfigFile'])

    def get(self, attr, section=None):
        if not section:
            section = 'MuchAway'
        try:
            result = self.config[section][attr]
        except:
            try:
                result = CONFIG[section][attr]
            except:
                result = None
        return result

class MuchAwayServer:
    def run(self):
        config = MuchAwayConfig()
        port = int(config.get('Port'))
        key_file = config.get('KeyFile')
        cert_file = config.get('CertFile')
        httpd = make_server('', port, MuchAwayApp(config))
        from ssl import wrap_socket
        httpd.socket = wrap_socket (
            httpd.socket,
            keyfile=key_file,
            certfile=cert_file,
            server_side=True
        )
        print("Serving on port {}...".format(port))
        httpd.serve_forever()

if __name__ == '__main__':
    server = MuchAwayServer()
    server.run()
