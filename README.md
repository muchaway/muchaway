Install notmuch
===============
```
sudo aptitude install notmuch
```

For each user do:
```
HOME="/srv/mail/probeta.net/sim6"
NOTMUCH_CONF="$HOME/.notmuch-config"
sudo -u mail touch "$NOTMUCH_CONF"

sudo -u mail notmuch --config="$NOTMUCH_CONF" config set user.name "Simó Albert i Beltran"
sudo -u mail notmuch --config="$NOTMUCH_CONF" config set user.primary_email "sim6@probeta.net"
sudo -u mail notmuch --config="$NOTMUCH_CONF" config set database.path "$HOME/Maildir"
sudo -u mail notmuch --config="$NOTMUCH_CONF" config set search.exclude_tags "deleted;spam;"

sudo -u mail notmuch --config="$NOTMUCH_CONF" new
```

Install MySQL
=============
```
sudo aptitude install mysql-server
cat << EOF | mysql -u root -p
CREATE DATABASE accounts;
USE accounts;
CREATE TABLE accounts (
       user varchar(128) NOT NULL,
       domain varchar(128) NOT NULL,
       password varchar(128) NOT NULL,
       PRIMARY KEY (user, domain)
)
EOF
```

For each user do:
```
cat << EOF | mysql -u root -p accounts
INSERT INTO accounts VALUES ('sim6', 'probeta.net', ENCRYPT('password'))
EOF
```

Install muchaway
================
```
sudo aptitude install python3-pymysql
sudo mkdir /etc/muchaway
sudo openssl req -new -x509 -keyout /etc/muchaway/server.key -out /etc/muchaway/server.pem -days 365 -nodes
cat << EOF | sudo tee -a /etc/muchaway/muchaway.ini
[MuchAway]
Port = 443
CertFile = /etc/muchaway/server.pem
KeyFile = /etc/muchaway/server.key
MailPath = /srv/mail
NotmuchConfig = .notmuch-config
NotmuchCmd = notmuch
AuthBackend = MySQL

[MySQL]
Host = localhost
User = user
Password = password
DataBase = accounts
Table = accounts
FieldUser = user
FieldDomain = domain
FieldPassword = password
EOF
sudo wget -O /usr/local/bin/muchaway https://gitlab.com/muchaway/muchaway/raw/master/muchaway.py
sudo chmod +x /usr/local/bin/muchaway
```

Run muchaway
============
```
sudo -u mail muchaway
```

Usage
=====
You can use it like notmuch CLI:
```
wget --user=sim6@probeta.net --password=password "https://example.com/help
wget --user=sim6@probeta.net --password=password "https://example.com/show --format=json tag:inbox"
```
